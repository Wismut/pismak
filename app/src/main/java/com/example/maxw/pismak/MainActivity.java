package com.example.maxw.pismak;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final LinearLayout container = findViewById(R.id.button_container);

		try {
			JSONArray jsonArray = Util.getJSONArrayFromInputStream(getAssets().open(ThisApp.FILE_NAME));
			LinkedHashSet<Question> questionSet = Question.getQuestionSet(jsonArray, ThisApp.QUESTION_COUNT);
			ThisApp.QUESTION_LIST = new ArrayList<>(questionSet);
			for (int i = 0; i < ThisApp.BUTTON_COUNT; i++) {
				PismakButton pismakButton = (PismakButton) LayoutInflater.from(this).inflate(R.layout.pismak_button, container, false);
				pismakButton.setText(ThisApp.QUESTION_LIST.get(ThisApp.CURRENT_QUESTION).getVariants().get(i));
				pismakButton.isCorrect = ThisApp.QUESTION_LIST.get(ThisApp.CURRENT_QUESTION).getCorrVariant().equals(pismakButton.getText());
				container.addView(pismakButton);
			}
			ThisApp.CURRENT_QUESTION++;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
