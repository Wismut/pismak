package com.example.maxw.pismak;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Util {
	public static JSONArray getJSONArrayFromInputStream(InputStream inputStream) {
		try {
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[16384];
			while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();
			return new JSONArray(new String(buffer.toByteArray()));
		} catch (IOException | JSONException e) {
			throw new RuntimeException();
		}
	}
}
