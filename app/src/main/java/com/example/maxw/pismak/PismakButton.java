package com.example.maxw.pismak;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


public class PismakButton extends android.support.v7.widget.AppCompatButton {

	public boolean isCorrect;

	{
		final AppCompatButton button = this;
		setAllCaps(false);
		this.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (isCorrect) {
					button.setBackgroundResource(R.drawable.background_button_right);
					ThisApp.MARK++;
				} else {
					final LinearLayout container = (LinearLayout) button.getParent();
					for (int i = 0; i < container.getChildCount(); i++) {
						Log.d("type", container.getChildAt(i).getClass().getName());
						PismakButton currentButton = (PismakButton) container.getChildAt(i);
						if (currentButton.isCorrect) {
							currentButton.setBackgroundResource(R.drawable.background_button_right);
							break;
						}
					}
					button.setBackgroundResource(R.drawable.background_button_wrong);
				}
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						askNextQuestion(button);
					}
				}, ThisApp.PAUSE);
			}
		});
	}

	private void askNextQuestion(AppCompatButton button) {
		if (ThisApp.CURRENT_QUESTION != 10) {
			QuestionFragment fragment = new QuestionFragment();
			FragmentManager manager = ((Activity) getContext()).getFragmentManager();
			FragmentTransaction transaction = manager.beginTransaction();
			transaction.replace(R.id.layout, fragment);
			transaction.addToBackStack(null);
			manager.popBackStack();
			transaction.commit();
			final LinearLayout container = (LinearLayout) button.getParent();
			for (int i = 0; i < container.getChildCount(); i++) {
				PismakButton currentButton = (PismakButton) container.getChildAt(i);
				currentButton.setText(ThisApp.QUESTION_LIST.get(ThisApp.CURRENT_QUESTION).getVariants().get(i));
				currentButton.isCorrect = ThisApp.QUESTION_LIST.get(ThisApp.CURRENT_QUESTION).getCorrVariant().equals(currentButton.getText());
				currentButton.setBackgroundResource(R.drawable.background_button_default);
			}
			ThisApp.CURRENT_QUESTION++;
		} else {
			ResultFragment fragment = new ResultFragment();
			FragmentManager manager = ((Activity) getContext()).getFragmentManager();
			FragmentTransaction transaction = manager.beginTransaction();
			transaction.replace(R.id.layout, fragment);
			transaction.addToBackStack(null);
			manager.popBackStack();
			transaction.commit();
			final TextView textView = ((Activity) getContext()).findViewById(R.id.question);
			textView.setText("Ви набрали " + ThisApp.MARK + " баллів");
			final LinearLayout container = (LinearLayout) button.getParent();
			PismakButton buttonNewGame = (PismakButton) container.getChildAt(0);
			buttonNewGame.setText("Почати нову гру");
			buttonNewGame.setBackgroundResource(R.drawable.background_button_default);
			buttonNewGame.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent intent = new Intent(getContext(), MainActivity.class);
					getContext().startActivity(intent);
					((Activity) getContext()).finish();
				}
			});
			container.getChildAt(1).setVisibility(INVISIBLE);
			container.getChildAt(2).setVisibility(INVISIBLE);
			PismakButton exitButton = (PismakButton) container.getChildAt(3);
			exitButton.setBackgroundResource(R.drawable.background_button_default);
			exitButton.setText("Вихід");
			exitButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					((Activity) getContext()).finish();
				}
			});
			ThisApp.CURRENT_QUESTION = 0;
			ThisApp.MARK = 0;
		}
	}

	public PismakButton(Context context) {
		super(context);
	}

	public PismakButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PismakButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
}
