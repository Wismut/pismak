package com.example.maxw.pismak;

import android.app.Application;

import java.util.ArrayList;

public class ThisApp extends Application {
	public static final String FILE_NAME = "questions.json";
	public static final int QUESTION_COUNT = 10;
	public static final int BUTTON_COUNT = 4;
	public static ArrayList<Question> QUESTION_LIST;
	public static int CURRENT_QUESTION = 0;
	public static final int PAUSE = 2000;
	public static int MARK = 0;
}
