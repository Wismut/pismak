package com.example.maxw.pismak;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONObject;

public class QuestionFragment extends Fragment {

	JSONObject object;

	public QuestionFragment(JSONObject object) {
		this.object = object;
		Log.d("con", "fra");
	}

	public QuestionFragment() {
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		Log.d("oncreate", "fragment2");
		View rootView = inflater.inflate(R.layout.fragment_question, container, false);
		for (int i = 0; i < ThisApp.BUTTON_COUNT; i++) {
			View button = LayoutInflater.from(getActivity()).inflate(R.layout.pismak_button, (ViewGroup) rootView, false);
			((ViewGroup) rootView).addView(button);
		}
		return rootView;
	}
}
