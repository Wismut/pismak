package com.example.maxw.pismak;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class Question {
	final private int id;
	final private List<String> variants;
	final private String corrVariant;
	final private String rule;

	public int getId() {
		return id;
	}

	public String getCorrVariant() {
		return corrVariant;
	}

	public String getRule() {
		return rule;
	}

	public List<String> getVariants() {
		return variants;
	}

	public Question(JSONObject object) {
		try {
			this.id = object.getInt("id");
			this.variants = new ArrayList<>();
			this.variants.add(object.getString("variant1"));
			this.variants.add(object.getString("variant2"));
			this.variants.add(object.getString("variant3"));
			this.variants.add(object.getString("variant4"));
			this.corrVariant = object.getString("corrVariant");
			this.rule = object.getString("rule");
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	public static LinkedHashSet<Question> getQuestionSet(JSONArray array, int length) {
		LinkedHashSet<Question> set = new LinkedHashSet<>();
		while (set.size() < length) {
			try {
				set.add(new Question(array.getJSONObject((int) (Math.random() * array.length()))));
			} catch (JSONException e) {
				throw new RuntimeException(e);
			}
		}
		return set;
	}
}
